/** @type {import('tailwindcss').Config} */
module.exports = {
	content: [
		'./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue,yaml}',
		'./node_modules/flowbite/**/*.js'
	],
	theme: {
		extend: {},
	},
	variants: {
		extend: {
			visibility: ["group-hover"],
		},
	  },
	//important: '#rps-header.rps-header',
	plugins: [
		require('@tailwindcss/typography'),
		require('flowbite/plugin'),
	],
}