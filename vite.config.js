import { resolve } from 'path'
import { defineConfig } from 'vite'

export default defineConfig({
    build: {
        lib: {
            entry: resolve(__dirname, 'src/header/header.js'),
            name: 'rpsHeader',
            // the proper extensions will be added
            fileName: 'header',
            formats: ['es'],
        },
    },
})
