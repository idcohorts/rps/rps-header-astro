export function onRequest (context, next) {
    // intercept data from a request
    // optionally, modify the properties in `locals`
    context.locals.token = "here be tokens";

    // return a Response or the result of calling `next()`
    return next();
};
