---
layout: ../layouts/markdown.astro
---

# How to include the Header into your service

Add a **script element** in the head of your HTML document:
```javascript
<script type="text/javascript" src="$RPS_HEADER_JS_URL"></script>
```

Add a **header element** for the header in the body of your HTML document:
```javascript
<header id="rps-header" class="rps-header"></header>
```

(recommended) Add a hint in form of a **data-user-logged-in attribute** for the header with the current login (true or false) status according to your application:
```javascript
<header id="rps-header" class="rps-header" data-user-logged-in="true"></header>
```

(optional) Add a footer element for the footer in the body of your HTML document:
```javascript
<footer id="rps-footer" class="rps-footer"></footer>
```

Notify the <a href="$RPS_HEADER_ADMIN_CONTACT_LINK">Administrators of the RPS</a> that they should add your service to <tt>rps_header_allowed_origin</tt> in the Ansible site config repo so the login status on your site will be displayed properly.

*look up the source of this page to see an example*
