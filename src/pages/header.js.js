export async function GET({ params, redirect }) {
    if (import.meta.env.DEV) {
        return redirect("/src/header/dev.js", 307);
    } else {
       // throw new Error('Not Implemented!');
        return import.meta.glob("../header/header.js");
    } 
}
