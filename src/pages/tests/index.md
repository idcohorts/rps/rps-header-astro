---
layout: ../../layouts/markdown.astro
---
# Header Tests
- [login-hint test page](login-hint)
- [bratty test page](brat)
- [margins test page](margins)
- [old nav element](old-nav-element)
- [lorem ipsum](lorem-ipsum)

## Login Endpoints
- [sign_in](/oauth2/sign_in?rd=/tests/)
- [sign_out](/oauth2/sign_out)
- [userinfo](/oauth2/userinfo)
