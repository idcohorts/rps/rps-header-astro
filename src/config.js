import yaml from 'yaml-js';
import { promises as fs } from 'fs';

export default async function config() {
    const configFile = await fs.readFile('config.yaml', 'utf-8');
    return yaml.load(configFile);
}
