// This script injects the dev header source as a module script tag
const me = document.currentScript;
const scriptSrcURL = new URL(me.src);
const baseURL = scriptSrcURL.origin;
const newScriptSrc = `${baseURL}/src/header/header.js`
var devScript = document.createElement('script');
devScript.setAttribute('src',newScriptSrc);
devScript.setAttribute('type','module');
console.log("[RPS Header dev] Injecting devScript...",devScript)
me.replaceWith(devScript)
console.log("[RPS Header dev] devScript injected.")
