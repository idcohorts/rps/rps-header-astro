import loaderHTML from './loader.html?raw'

console.log("[RPS Header] loading...");

// Config
function getBaseURLfromScript() {
  let scriptSrcURL;
  if (typeof document !== 'undefined' && document.currentScript) {
    scriptSrcURL = new URL(document.currentScript.src);
  } else {
    scriptSrcURL = new URL(import.meta.url);
  }
  return scriptSrcURL.origin;
}
const baseURL = getBaseURLfromScript();
const clientConfigDefault = {
  useShaddowDom: false,
  listenToLocationChanges: true,
  headerContentURL: `${baseURL}/header`,
  createHeaderElementIfMissing: false,
};
const clientConfig = { ...clientConfigDefault };
console.log("[RPS Header] clientConfig:", clientConfig);

// Outer Style
const globalStyleElement = document.createElement("style");
globalStyleElement.id = "rps-header-global-style";
//globalStyleElement.textContent = {% filter tojson %}{% include 'global.css.jinja' %}{% endfilter %};

// Header Container
const headerContainer = document.createElement("div");
headerContainer.id = "rps-header-container";
headerContainer.classList.add("rps-header-container");

// Header Style
const headerStyleElement = document.createElement("style");
headerStyleElement.id = "rps-header-content-style";
// headerStyleElement.textContent =

// Header Content
const headerContentElement = document.createElement("div");
headerContentElement.id = "rps-header-content";
headerContentElement.classList.add("rps-header-content");
headerContentElement.setAttribute("data-bs-theme", "light");
headerContentElement.innerHTML = loaderHTML;

// Add header to Shadow DOM or to the Header Container
if (clientConfig.useShaddowDom) {
  // Shadow DOM (it is bigger on the inside than on the outside :D)
  const headerShadowDimension = headerContainer.attachShadow({ mode: "open" });
  headerShadowDimension.innerHTML =
    "<!-- [RPS Header] Shadow Dimension (it is bigger on the inside than on the outside :D) -->";
  headerShadowDimension.appendChild(headerStyleElement);
  headerShadowDimension.appendChild(headerContentElement);
} else {
  headerContainer.appendChild(headerStyleElement);
  headerContainer.appendChild(headerContentElement);
}

function fetchHeaderContent() {
  fetch(clientConfig.headerContentURL)
    .then((response) => {
      response
        .text()
        .then((text) => {
          headerContentElement.innerHTML = text;
          console.log("[RPS Header] Content loaded.");
        })
        .catch((error) => {
          headerContentElement.innerHTML =
            '<p style="color: red; font-style:italic;">[RPS Header] Error while parsing content</p>';
          throw new Error("[RPS Header] Error while parsing content", error);
        });
    })
    .catch((error) => {
      headerContentElement.innerHTML =
        '<p style="color: red; font-style:italic;">[RPS Header] Error while fetching content</p>';
      throw new Error("[RPS Header] Error while fetching content", error);
    });
}
fetchHeaderContent();

// listen to window.location changes
if (clientConfig.listenToLocationChanges) {
  window.addEventListener("popstate", function (event) {
    fetchHeaderContent();
  });
}

function createHeaderElement(){
  const headerElement = document.createElement("header");
  headerElement.id = "rps-header";
  headerElement.classList.add("rps-header");
  document.body.prepend(headerElement);
  return headerElement
}

const headerElementPromise = new Promise((resolve, reject) => {
  const headerElement = document.getElementById("rps-header");
  if (headerElement) {
    console.log("[RPS Header] Found header element in DOM when script has been run.");
    resolve(headerElement);
    return;
  }
  document.addEventListener("readystatechange", () => {
    const headerElement = document.getElementById("rps-header");
    if (headerElement) {
      console.log(`[RPS Header] Found header element in DOM when document reached ${document.readyState} readyState.`);
      resolve(headerElement);
    } else {
      console.error(`[RPS Header] Header element has not been found in DOM when document reached ${document.readyState} readyState.`);
      if (clientConfig.createHeaderElementIfMissing){
        resolve(createHeaderElement());
      } else (
        reject("Header element has not been found in DOM.")
      )
    }
  });
});

headerElementPromise
  .then((headerElement) => {
    if (headerElement.nodeName === "HEADER") {
      headerElement.appendChild(globalStyleElement);
      headerElement.appendChild(headerContainer);
    } else {
      headerElement.innerHTML =
        "<p style=\"color: red; font-style:italic;\">[RPS Header] Element with id 'rps-header' is not a header element</p>";
      console.error("[RPS Header] Element with id 'rps-header' is not a header element");
    }
  })
  .catch((error) => {
    throw new Error(error);
  });

// listen for new header elements appearing in the DOM
// const bodyObserver = new MutationObserver((mutationList, observer) => {
//   for (const mutation of mutationList) {
//     if (mutation.type === "childList") {
//       console.log("A child node has been added or removed.",mutation.addedNodes);
//     } else if (mutation.type === "attributes") {
//       console.log(`The ${mutation.attributeName} attribute was modified.`);
//     }
//   }
// });
// bodyObserver.observe(document.body, {
//   childList: true,
//   // subtree: true,
// });
