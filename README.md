# How to use

## Inject header to other services

The idea of this project is to have the RPS header served as simple as inserting one line of code into HTML. Assuming the Header service running at https://{header_domain}/, the injection code should be 

```<script type="module" src="https://{header_domain}s/header.js"></script>```

For that reason it is highly recommended to develop the RPS header service together with some other service deployed in the network. This gives a live vision on how good the integration works.

## Develop

Currently the script injects the code that is retrieved from the Astro page at ```http://{header_domain}/header```

The content of the page is read from data files at `./src/components/data/*`

Avoid hardcoding the styles, the contents and etc., use structured data files!

# Build

## Build from scratch:

```docker compose build```

## Start ad a daemon:

```docker compose up -d```

# Deploy
## Deploy dev:

In development, use `yarn run dev` option

## Deploy prod:

In production, use `yarn run server` option


