import { defineConfig } from 'astro/config';
import node from '@astrojs/node';
import mdx from '@astrojs/mdx';
import tailwind from "@astrojs/tailwind";

// https://astro.build/config
export default defineConfig({
  server: {
    port: 4080,
    host: true
  },
  integrations: [
    mdx(), 
    tailwind(),
  ],
  output: 'server',
  adapter: node({
    mode: 'standalone',
  }),
  vite: {
    build: {
      rollupOptions: {
        input: {
          header: 'src/header/header.js',
        },

      },
      lib: {
        entry: 'src/header/header.js',
        name: 'rpsHeader',
        fileName: 'header',
        formats: ['es'],
      },
    },
  }
});
